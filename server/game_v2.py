from collections import OrderedDict
import numpy as np
import json
from flask_socketio import emit

N_SPIES = {
    5 : 2,
    6 : 2,
    7 : 3,
    8 : 3,
    9 : 3,
    10 : 4
}

MISSION_TEAM_SIZES_BY_PHASE = [
    [2,2,2,3,3,3],
    [3,3,3,4,4,4],
    [2,4,3,4,4,4],
    [3,3,4,5,5,5],
    [3,4,4,5,5,5]
]

STATES = (
    'WAITING FOR GAME START',
    'WAITING FOR TEAM SELECTION',
    'WAITING FOR TEAM VOTES',
    'WAITING FOR SUCCESS VOTES'
)


class Board:
    def __init__(self, n_players):
        self.n_players = n_players
        self.team_vote_counter = 0
        self.round = 0
        self.win_history = []

    def increment_vote_count(self):
        self.team_vote_counter += 1

    def increment_round_count(self):
        self.round += 1

    def get_team_size(self):
        s = MISSION_TEAM_SIZES_BY_PHASE[self.round-1][self.n_players-5]
        return s

    def add_win(self, team):
        self.win_history.append(team)


class Player:
    def __init__(self, player_name, session_id):
        self.name = player_name
        self.role = None
        self.team_vote = None
        self.success_vote = None
        self.session_id = session_id

    def set_team_vote(self, vote):
        self.team_vote = vote

    def set_success_vote(self, vote):
        self.success_vote = vote

    def set_role(self, role):
        self.role = role

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name


class Game:
    def __init__(self):
        self.players = []
        self.mission_team = []
        self.state = STATES[0]
        self.leader_idx = None
        self.board = None

    #
    # PRIVATE METHODS
    #

    def _assign_roles(self):
        n_players = len(self.players)
        n_spies = N_SPIES[n_players]

        # Pick N spies and set their status
        for i in np.random.choice(n_players, n_spies, replace=False):
            self.players[i].set_role('spy')

        # Set other players to resistance
        for p in self.players:
            if not p.role == 'spy':
                p.set_role('resistance')

    def _everyone_voted_team(self):
        return [p.team_vote is not None for p in self.players].count(True) == len(self.players)

    def _everyone_voted_success(self):
        result = [p.success_vote is not None for p in self.mission_team].count(True) == len(self.mission_team)
        return result 

    def _vote_passed(self):
        votes = [p.team_vote for p in self.players]
        pro = votes.count(True)
        con = votes.count(False)
        self.console_log(f'VOTE COMPLETE: {pro} for and {con} against!')
        if pro > con:
            self.console_log(f'TEAM SUCCESFULLY SELECTED: {", ".join([p.name for p in self.mission_team])}')
            self._team_selected_successfully()
            return True
        else:
            self.console_log('Failed to select team.')
            self._team_not_selected()
            return False

    def _team_selected_successfully(self):
        self.state = STATES[3]
        self.board.team_vote_counter = 1
        self.leader_idx = (self.leader_idx + 1) % len(self.players)
        self._reset_player_votes()
    
    def _team_not_selected(self):
        self.state = STATES[1]
        self.board.increment_vote_count()
        self.leader_idx = (self.leader_idx + 1) % len(self.players)
        self.console_log(f'New leader is {self.players[self.leader_idx].name}')
        self._reset_player_votes()

    def _mission_success(self):
        self.board.add_win('resistance')
        self.console_log('MISSION SUCCESS!!!!!!')
        self.board.increment_round_count()
        self.console_log(f'STARTING ROUND {self.board.round}')
        self.leader_idx = (self.leader_idx + 1) % len(self.players)
        self.console_log(f'New leader is {self.players[self.leader_idx].name}')
        if not self.end_game():
            self.board.team_vote_counter = 1
            self.state = STATES[1]
        else:
            quit()

    def _mission_failed(self):
        self.console_log('MISSION FAILED!!!!!!')
        self.board.increment_round_count()
        self.board.add_win('spies')
        if not self.end_game():
            self.board.team_vote_counter = 1
            self.state = STATES[1]
        else:
            quit()

    def _reset_player_votes(self):
        for player in self.players:
            player.team_vote = None
            player.success_vote = None

    def _get_leader(self):
        return self.players[self.leader_idx]

    #  PUBLIC METHODS
    #

    def console_log(self, message):
        try:
            emit('console', message, broadcast=True)
        except Exception as e:
            print(e)

    def suggest_team(self, player, member_list):
        if player == self.players[self.leader_idx].name and self.state == STATES[1]:
            self.mission_team = []
            if len(member_list) == self.board.get_team_size():
                for player_name in member_list:
                    self.mission_team.append(self.find_player(player_name))
                self.console_log(f'Leader {self.players[self.leader_idx].name} suggested team: {", ".join([p.name for p in self.mission_team])}')
                self.state = STATES[2]

    def cast_vote_for_team(self, player_name, vote):
        if self.state == STATES[2]:
            self.find_player(player_name).set_team_vote(vote)
            self.console_log(f'{player_name} voted for team')
            #if self._everyone_voted_team():
                # return self._vote_passed()


    def reveal_team_vote_result(self):
        self._vote_passed()

    def reveal_mission_result(self):
        if self._everyone_voted_success():
            if any([p.success_vote == False for p in self.mission_team]):
                self._mission_failed()
            else:
                self._mission_success()

    def cast_vote_for_success(self, player_name, vote):
        if self.state == STATES[3] and player_name in [p.name for p in self.mission_team]:
            self.find_player(player_name).set_success_vote(vote)
            self.console_log(f'{player_name} voted for mission success/failure')
            # if self._everyone_voted_success():
            #     if any([p.success_vote == False for p in self.mission_team]):
            #         self._mission_failed()
            #     else:
            #         self._mission_success()

    def end_game(self):
        if self.board.round == 6:
            if self.board.win_history.count('resistance') > self.board.win_history.count('spies'):
                self.console_log('GAME OVER! THE RESISTANCE WINS!')
            else:
                self.console_log('GAME OVER! THE SPIES WIN!')
            
            new_game = Game()
            [new_game.add_player(p.name, p.session_id) for p in self.players]
            self.__dict__.update(new_game.__dict__)
            emit('endgame', 'restart', broadcast=True)
            emit('game-state', self.dump_state(), broadcast=True)
        else:
            return False

    def reset_round(self):
        self._reset_player_votes()
        self.state = STATES[1]

    def reset(self):
        new_game = Game()
        self.__dict__.update(new_game.__dict__)

    def find_player(self, name):
        for p in self.players:
            if p.name == name:
                return p
        return None

    def find_by_id(self, session_id):
        for p in self.players:
            if p.session_id == session_id:
                return p

    def start(self):
        if self.state == STATES[0]:
            self.console_log('STARTING GAME')
            self._assign_roles()
            self.board = Board(len(self.players))
            self.board.increment_round_count()
            self.board.increment_vote_count()
            self.state = STATES[1]
            self.leader_idx = np.random.choice(len(self.players), 1)[0]

    def add_player(self, name, session_id):
        self.players.append(Player(name, session_id))
        self.console_log(f'Player {name} has joined the game!')

    def remove_player(self, name):
        del self.players[name]
        self.console_log(f'Removed player {name}')

    def dump_state(self):
        json_file = {}
        if self.board is not None:
            json_file['board'] = self.board.__dict__
            json_file['board']['team_size'] = self.board.get_team_size()
        json_file['players'] = {}
        json_file['mission_team'] = []
        json_file['resistance_wins'] = 0
        json_file['spy_wins'] = 0
        if len(self.players) > 0:
            json_file['team_votes'] = len([p.team_vote for p in self.players if p.team_vote is not None])
        if len(self.mission_team) > 0:
            json_file['success_votes'] = len([p.success_vote for p in self.players if p.success_vote is not None])
        for key, value in self.__dict__.items():
            if key == 'leader_idx' and value is not None:
                json_file['leader'] = self._get_leader().name
            elif isinstance(value, str):
                json_file[key] = value
            elif isinstance(value, list):
                for item in value:
                    if isinstance(item, Player):
                        if key == 'players':
                            json_file['players'][item.name] =  item.__dict__
                            
                        elif key == 'mission_team':
                            json_file['mission_team'].append(item.name)
            elif isinstance(value, Board):
                for item in value.win_history:
                    if item =='resistance':
                        json_file['resistance_wins'] += 1
                    elif item =='spies':
                        json_file['spy_wins'] += 1
        return json.dumps(json_file)
    

if __name__ == "__main__":
    '''Set up a new game'''
    g = Game()
    print(g.__dict__)
    '''Add players'''
    players = ['Alan', 'Kristian', 'Jes', 'Meg', 'Brad']
    for p in players:
        g.add_player(p, None)

    '''Start game'''
    g.start()
    print(g.board.__dict__)
    print(g.board.get_team_size())

    for rnd in range(1, 6):
        print(f'<<<<<<<<<<<ROUND {rnd}>>>>>>>>>>>>')

        print(g.__dict__)
        print(g.board.__dict__)

        '''Player 1 suggests a team'''
        print(g._get_leader())
        # print(g.board.get_team_size())

        g.suggest_team(g._get_leader().name, np.random.choice(players, g.board.get_team_size(), replace=False))
        print(type(g.leader_idx))

        print(g.__dict__)
        print(g.board.__dict__)

        '''All players vote'''
        for player in players:
            print(g.state)
            g.cast_vote_for_team(player, True)

        print(g.dump_state())
        print(g.__dict__)
        print(g.board.__dict__)

        '''Team is selected and members vote for success/fail'''
        for player in g.mission_team:
            g.cast_vote_for_success(player.name, True)

        print(g.__dict__)
        print(g.board.__dict__)
