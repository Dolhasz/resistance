
var socket = io('/');

function isEmpty(obj) {
  for (var prop in obj) {
    if (obj.hasOwnProperty(prop)) {
      return false;
    }
  }

  return JSON.stringify(obj) === JSON.stringify({});
}

function resetData() {
  return {
    userName: '',
    submittedName: '',
    players: [],
    gameState: {},
    userRole: '',
    isActive: {},
    team_votes: 0,
    console: [],
    teamVoteCast: null,
    missionVoteCast: null,
    sounds: {
      playerJoined: [new Audio('/static/sounds/km/player_join.m4a'), new Audio('/static/alan_sounds/player_joined.mp3')],
      gameStart: [new Audio('/static/sounds/km/game_started.m4a'), new Audio('/static/alan_sounds/game_start.mp3')],
      teamVote: [new Audio('/static/sounds/km/team_vote.m4a'), new Audio('/static/alan_sounds/mission_vote.mp3')],
      missionVote: [new Audio('/static/sounds/km/mission_vote.m4a'), new Audio('/static/alan_sounds/mission_vote.mp3')],
      sucess: [new Audio('/static/sounds/km/resistance_win.m4a'), new Audio('/static/alan_sounds/resistance_wins.mp3')],
      fail: [new Audio('/static/sounds/km/spies_win.m4a'), new Audio('/static/alan_sounds/spies_win.mp3')],
    }
  }
}


// Getting all types of messages sent using the socket.send() method
socket.on('message', function (msg) {
  console.log('Message ' + msg);
});


var app = new Vue({
  el: '#app',
  delimiters: ["[[", "]]"],
  data: resetData(),
  created() {
    // Get list of current online users when starting the app
    
    axios
      .get('/players')
      .then(response => (this.players = response.data['players']))

    // Get object with players
    // {"players": ["Alan", "Kristian", "Brad", "Meg", "Jes"]}
    socket.on('new-player', (data) => {
      const playerJoinedGameSound = this.sounds.playerJoined[Math.floor(Math.random() * this.sounds.playerJoined.length)];
      playerJoinedGameSound.play();

      console.log(data)
      players = JSON.parse(data);
      this.players = players['players']
      this.players.forEach(x => {
        this.$set(this.isActive, x, false)
      })
    });

    socket.on('game-state', (incoming) => {
      if (this.gameState.state === 'WAITING FOR GAME START' && incoming.state === 'WAITING FOR TEAM SELECTION') {
        let gameStartSound = this.sounds.gameStart[Math.floor(Math.random() * this.sounds.gameStart.length)];
        gameStartSound.play();
      }
      if (this.gameState.state === 'WAITING FOR TEAM SELECTION' && incoming.state === 'WAITING FOR TEAM VOTES') {
        let teamVOteSound = this.sounds.teamVote[Math.floor(Math.random() * this.sounds.teamVote.length)];
        teamVOteSound.play();
      }
      if (this.gameState.state === 'WAITING FOR TEAM VOTES' && incoming.state === 'WAITING FOR SUCCESS VOTES') {
        let missionVOteSound = this.sounds.missionVote[Math.floor(Math.random() * this.sounds.missionVote.length)];
        missionVOteSound.play();
      }
      this.gameState = JSON.parse(incoming)
      //console.log(this.countTeamVotes(this.gameState))
    });
    
    socket.on('game-reset', (incoming) => {
      Object.assign(this.$data, resetData());
    })

    socket.on('console', (data) => {
      const resistanceWin = this.sounds.sucess[Math.floor(Math.random() * this.sounds.sucess.length)];
      const spiesWin = this.sounds.fail[Math.floor(Math.random() * this.sounds.fail.length)];
      if (data.includes('SUCCESS')) {
        resistanceWin.play();
      }

      if (data.includes('FAIL')) {
        spiesWin.play();
      }

      var textArea = document.getElementById("message_box")
      var today = new Date();
      var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
      var time = ('0' + today.getHours()).slice(-2) + ':' + ('0' + today.getMinutes()).slice(-2) + ':' + ('0' + today.getSeconds()).slice(-2);
      textArea.value += '\n' + time + ' ' + data;
      textArea.scrollTop = textArea.scrollHeight
    });

    socket.on('endgame', (data) => {
      var textArea = document.getElementById("message_box")
      textArea.value += '\n' + 'NEW GAME STARTED!!!' + '\n\n';
      textArea.scrollTop = textArea.scrollHeight
      this.userRole = ''
      this.team_votes = 0
    });

    socket.on('user-role', (data) => {
      // alert(data)
      console.log(data)
      this.userRole = data
    });

  },

  // computed: {
  // },

  methods: {

    revealMissionTeamVoteResult: () => {
      console.log("REVEAL TEAM MISSION RESULT!")
      socket.emit('reveal-team-vote-result')
    },

    revealMissionResult: () => {
      console.log("REVEAL TEAM MISSION RESULT!")
      socket.emit('reveal-mission-result')
    },

    toggleActive(name) {
      this.isActive[name] = !this.isActive[name];
    },

    playerInMission() {
      return this.gameState.mission_team.find(x => x === this.submittedName)
    },

    sendGameName(playerName) {
      // Set submitted name
      this.submittedName = playerName
      // emit user name of player who has entered the game
      socket.emit('user-entered-game', {
        userName: this.userName,
      });
    },

    startGame() {
      const gameStartSound = this.sounds.gameStart[Math.floor(Math.random() * this.sounds.gameStart.length)];
      gameStartSound.play()
      socket.emit('start-game', true)
    },
    resetMatch() {
      socket.emit('start-game', 'match')
    },
    resetGame() {
      socket.emit('start-game', false)
    },

    teamVote(vote) {
      this.teamVoteCast = vote;
      socket.emit('team-vote', { 'player_name': this.submittedName, 'vote': vote })
    },

    missionVote(vote) {
      this.missionVoteCast = vote;
      socket.emit('mission-vote', { 'player_name': this.submittedName, 'vote': vote })
    },

    countMissionTeam() {
      Object.keys(this.isActive).reduce((tot, cur) => {
        if (this.isActive[cur]) {
          return tot + 1
        }
      }, 0)
    },

    suggestTeam() {
      var team = Object.keys(this.isActive).filter(k => {
        console.log(k)
        if (this.isActive[k]) {
          return k
        }
      })
      if (team.length == this.gameState.board.team_size) {
        socket.emit('suggest-team', JSON.stringify({ playerName: this.userName, team: team }))
      }
      else {
        console.log('NOT ENOUGH PLAYERS IN TEAM')
      }
      console.log(team)
    }
  }
})
