from flask import Flask, render_template, request
from flask_socketio import SocketIO, send, emit
import game_v2 as resistance
import json
import datetime


app = Flask(__name__, static_folder='static')
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app)
game = resistance.Game()


@app.route('/')
def index():
    return render_template('new_index.html')


@app.route('/players')
def players():
    return json.dumps({'players': [p.name for p in game.players]})


@socketio.on('message')
def handleMessage(msg):
	send(msg, broadcast=True)


@socketio.on('connect')
def handledConnect():
	print('Client connected')
	emit('game-state', game.dump_state(), broadcast=True)


@socketio.on('disconnect')
def handleDisconnect():
    print('Client disconnected')
    send(f"User disconnected {datetime.datetime.now()}", broadcast=True)


@socketio.on('user-entered-game')
def handle_my_custom_event(json_data):
    game.add_player(json_data['userName'], request.sid)
    emit('new-player', json.dumps({'players': [p.name for p in game.players]}), broadcast=True)
    emit('game-state', game.dump_state(), broadcast=True)


@socketio.on('start-game')
def handle_game_start(json_data):
    if json_data == True:
        game.start()
        emit('game-state', game.dump_state(), broadcast=True)     
        for player in game.players:
            emit('user-role', player.role, room=player.session_id)

    elif json_data == False:
        game.reset()
        state = game.dump_state()
        emit('game-reset', True, broadcast=True)
        emit('game-state', game.dump_state(), broadcast=True)

    elif json_data == 'match':
        game.reset_round()
        emit('game-state', game.dump_state(), broadcast=True)


@socketio.on('suggest-team')
def handle_suggest_vote(json_data):
    json_data = json.loads(json_data)
    game.suggest_team(json_data['playerName'], json_data['team'])
    emit('game-state', game.dump_state(), broadcast=True)


@socketio.on('team-vote')
def handle_tame_vote(json_data):
    is_team_selected = game.cast_vote_for_team(json_data['player_name'], json_data['vote'])
    emit('game-state', game.dump_state(), broadcast=True)


@socketio.on('reveal-team-vote-result')
def reveal_team_vote_result():
    game.reveal_team_vote_result()
    emit('game-state', game.dump_state(), broadcast=True)


@socketio.on('reveal-mission-result')
def reveal_team_vote_result():
    game.reveal_mission_result()
    emit('game-state', game.dump_state(), broadcast=True)


@socketio.on('mission-vote')
def handle_mission_vote(json_data):
    mission_successful = game.cast_vote_for_success(json_data['player_name'], json_data["vote"])
    emit('game-state', game.dump_state(), broadcast=True)



if __name__ == '__main__':
    socketio.run(app)
